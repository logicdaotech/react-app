import React, {Component} from 'react'
import Clock from './Clock';

class App extends Component{
  constructor(){
    super();

    this.state = {
      text: "приложение ожидает",
      btnam: "Получить",
      models: ["тут получим ответ"]
    };

  }
async GetData(e){
    var now = new Date().toLocaleTimeString();
    var retval = [];
    var url = 'http://localhost:3030/myservice';

    retval = await this.getFetchResult(url);
    if (!retval){
      this.setState({
        text: "Сервер не отвечает."
      });
      return retval;
    }
    console.log(retval);
    var models = retval.models

    console.log(models);

    this.setState({
      text: "приложение дождалось " + now,
      btnam: "Получить еще раз",
      models: models
    });
  }

  getFetchResult(url) {
    return fetch(url)
      .then(response => response.json())
      .then(response => response)
      .catch(function(err) {
        console.log(err);
        return false;
    });
  }


  render(){

    return (
      <div className="card">
        <div className="card-header">
          <Clock />
        </div>

        <div className="card-body">
          <h5 className="card-title">{this.state.text}</h5>
          <button className="btn btn-primary" onClick={(e) => this.GetData(e)}>
            {this.state.btnam}
          </button>
        </div>
        <div className="card-footer text-muted">
          <ul className="list-group">
            {this.state.models.map(item => (
                <p className="list-group-item" key={item}>{item}</p>
            ))}
          </ul>
        </div>
      </div>
    )
  }
}




export default App
